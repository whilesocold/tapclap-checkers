export default class Utils {
	static allocateMultiArray(size) {
		let array = []
		for (let i = 0; i < size; i++) {
			array[i] = [];
		}
		return array;
	}
}