import Field from './gameState/Field';
import Cell from "./gameState/Cell";
import CellKind from "./gameState/CellKind";
import ChipKind from "./gameState/ChipKind";
import Chip from "./gameState/Chip";

export default class GameState extends Phaser.State {
	preload() {
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.scale.setMinMax(400, 450, 400, 450);

		this.game.load.atlas('atlas_0', 'res/atlas_0.png', 'res/atlas_0.json', Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
	}

	create() {
		this.backgroundGroup = new Phaser.Group(this.game);
		this.game.add.group(this.backgroundGroup);

		this.field = new Field(this.game, this.actions);
		this.field.y = 100;
		this.game.add.group(this.field);

		// decorations
		let kind = CellKind.Black;
		for (let x = 0; x < this.field.columns; x++) {
			let cell = new Cell(this.game, kind);

			cell.x = x * cell.width;
			this.backgroundGroup.add(cell);

			kind = kind == CellKind.White ? CellKind.Black : CellKind.White;
		}

		let overlay = new Phaser.Graphics(this.game);
		overlay.beginFill(0x000000);
		overlay.drawRect(0, 0, this.game.width, this.backgroundGroup.height);
		overlay.endFill();
		overlay.alpha = 0.35;

		this.backgroundGroup.add(overlay);

		// FIXME: простите за говнокод, я спешил но сделаль :(
		this.time = 300;
		this.timer = this.game.time.events.loop(Phaser.Timer.SECOND, this.onTick, this);

		this.timerLabel = new Phaser.Text(this.game);
		this.timerLabel.anchor.set(0.5);
		this.timerLabel.align = 'center';
		this.timerLabel.boundsAlignH = 'center';
		this.timerLabel.boundsAlignV = 'middle';
		this.timerLabel.x = this.game.width / 2;
		this.timerLabel.y = this.backgroundGroup.height / 2;
		this.timerLabel.fill = '#ffffff';
		this.timerLabel.fontSize = 52;
		this.backgroundGroup.add(this.timerLabel);

		this.scoreWhiteImg = new Chip(this.game, ChipKind.White);
		this.scoreWhiteImg.anchor.set(0.5);
		this.scoreWhiteImg.scale.set(0.9);
		this.scoreWhiteImg.x = 50;
		this.scoreWhiteImg.y = this.backgroundGroup.height / 2;
		this.backgroundGroup.add(this.scoreWhiteImg);

		this.scoreWhiteLabel = new Phaser.Text(this.game);
		this.scoreWhiteLabel.anchor.set(0.5);
		this.scoreWhiteLabel.text = '0';
		this.scoreWhiteLabel.align = 'center';
		this.scoreWhiteLabel.boundsAlignH = 'left';
		this.scoreWhiteLabel.boundsAlignV = 'middle';
		this.scoreWhiteLabel.x = 120;
		this.scoreWhiteLabel.y = this.backgroundGroup.height / 2;
		this.scoreWhiteLabel.fill = '#ffffff';
		this.scoreWhiteLabel.fontSize = 52;
		this.backgroundGroup.add(this.scoreWhiteLabel);

		this.scoreBlackImg = new Chip(this.game, ChipKind.Black);
		this.scoreBlackImg.anchor.set(0.5);
		this.scoreBlackImg.scale.set(0.9);
		this.scoreBlackImg.x = this.game.width - 50;
		this.scoreBlackImg.y = this.backgroundGroup.height / 2;
		this.backgroundGroup.add(this.scoreBlackImg);

		this.scoreBlackLabel = new Phaser.Text(this.game);
		this.scoreBlackLabel.anchor.set(0.5);
		this.scoreBlackLabel.text = '0';
		this.scoreBlackLabel.align = 'center';
		this.scoreBlackLabel.boundsAlignH = 'right';
		this.scoreBlackLabel.boundsAlignV = 'middle';
		this.scoreBlackLabel.x = this.game.width - 120;
		this.scoreBlackLabel.y = this.backgroundGroup.height / 2;
		this.scoreBlackLabel.fill = '#ffffff';
		this.scoreBlackLabel.fontSize = 52;
		this.backgroundGroup.add(this.scoreBlackLabel);

		this.onTick();
	}

	// FIXME: простите за говнокод, я спешил но сделаль :(
	onTick() {
		if (--this.time >= 0) {
			let minutes = Math.floor(this.time / 60);
			let seconds = this.time % 60;

			if (minutes < 10) {
				minutes = '0' + minutes;

			} else {
				minutes = minutes.toString();
			}

			if (seconds < 10) {
				seconds = '0' + seconds;

			} else {
				seconds = seconds.toString();
			}

			this.timerLabel.text = minutes + ':' + seconds;

			this.scoreWhiteLabel.text = this.field.scoreWhite;
			this.scoreBlackLabel.text = this.field.scoreBlack;

		} else if (this.time == -1) {
			this.game.input.onUp.removeAll();

			let msg = 'Ничья!';

			if (this.field.scoreWhite > this.field.scoreBlack) {
				msg = 'Копы победили!'

			} else if (this.field.scoreWhite < this.field.scoreBlack) {
				msg = 'Бандиты победили!';
			}

			let text = new Phaser.Text(this.game);
			text.anchor.set(0.5);
			text.scale.set(10);
			text.alpha = 0;
			text.text = msg;
			text.align = 'center';
			text.boundsAlignH = 'center';
			text.boundsAlignV = 'middle';
			text.x = this.game.width / 2;
			text.y = this.game.height / 2 + this.backgroundGroup.height / 2;
			text.fill = '#ffffff';
			text.fontSize = 72;

			this.game.add.tween(text)
				.to({alpha: 1}, 300, Phaser.Easing.Linear.None, true);

			this.game.add.tween(text.scale)
				.to({x: 1, y: 1}, 300, Phaser.Easing.Linear.None, true);

			this.game.add.existing(text);
		}
	}
}
