import ChipKind from "./ChipKind";

export default class Chip extends Phaser.Sprite {
	constructor(game, kind, column, row) {
		super(game, 0, 0, 'atlas_0');

		this.kind = kind;
		this.column = column;
		this.row = row;

		this._crowned = false;

		this.animations.add(ChipKind.White, ['zombie_01.png'], 1, true);
		this.animations.add(ChipKind.Black, ['zombie_02.png'], 1, true);
		this.animations.play(kind);
	}

	playAttention(delay, callback) {
		let y = this.y;

		this.game.add.tween(this)
			.to({y: y - 20}, 60, Phaser.Easing.Linear.None, false, delay)
			.to({y: y}, 60, Phaser.Easing.Linear.None, false)
			.start()
			.onComplete.add(() => {
			if (callback) {
				callback();
			}
		});
	}

	playFadeOff(callback) {
		this.game.add.tween(this)
			.to({alpha: 0}, 60, Phaser.Easing.Linear.None, false)
			.start()
			.onComplete.add(callback);

		this.game.add.tween(this.scale)
			.to({x: 0, y: 0}, 60, Phaser.Easing.Linear.None, false)
			.start();
	}

	set crowned(value) {
		this._crowned = value;
		this.tint = 0xff0000;
	}

	get crowned() {
		return this._crowned;
	}
}