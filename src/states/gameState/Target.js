export default class Target extends Phaser.Sprite {
	constructor(game) {
		super(game, 0, 0, 'atlas_0');

		this.anchor.set(0.5);

		this.animations.add('selector', ['selector.png'], 1, false);
		this.animations.play('selector');
	}

	show() {
		let time = 125;

		this.alpha = 0;
		this.scale.set(0, 0);

		this.game.add.tween(this.scale)
			.to({x: 1, y: 1}, time, Phaser.Easing.Linear.Out, true);

		this.game.add.tween(this)
			.to({alpha: 1}, time, Phaser.Easing.Linear.Out, true);
	}

	hide() {
		let time = 125;

		this.game.add.tween(this.scale)
			.to({x: 0, y: 0}, time, Phaser.Easing.Linear.In, true);

		this.game.add.tween(this)
			.to({alpha: 0}, time, Phaser.Easing.Linear.In, true);
	}
}