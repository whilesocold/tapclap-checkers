import 'underscore';

import TeamKind from './TeamKind';
import CellKind from "./CellKind";
import Cell from "./Cell";
import Utils from "../../utils/Utils";
import Chip from "./Chip";
import Target from "./Target";
import ActionKind from "./actions/ActionKind";
import SelectAction from "./actions/SelectAction";
import MoveAction from "./actions/MoveAction";

export default class Field extends Phaser.Group {
	constructor(game) {
		super(game);

		this.rows = 8;
		this.columns = 8;

		this.cellWidth = 100;
		this.cellHeight = 100;

		this.chips = [];
		this.cells = Utils.allocateMultiArray(this.rows);

		this.currTeam = TeamKind.White;

		this.scoreWhite = 0;
		this.scoreBlack = 0;

		this.currChip = null;

		this.actions = [];
		this.actionIdx = -1;
		this.doingAction = false;

		this.cellsGroup = new Phaser.Group(game);
		this.add(this.cellsGroup);

		this.startTarget = new Target(game);
		this.startTarget.alpha = 0;
		this.add(this.startTarget);

		this.endTarget = new Target(game);
		this.endTarget.alpha = 0;
		this.add(this.endTarget);

		this.chipsGroup = new Phaser.Group(game);
		this.add(this.chipsGroup);

		game.input.onUp.add((pointer) => this.onMouseUp(pointer));

		this.initCells();

		this.initChips(TeamKind.White);
		this.initChips(TeamKind.Black);

		this.playAttentionChips();
	}

	initCells() {
		let kind = CellKind.White;

		for (let row = 0; row < this.rows; row++) {
			for (let column = 0; column < this.columns; column++) {
				let cell = new Cell(this.game, kind, column, row);

				cell.x = column * this.cellWidth;
				cell.y = row * this.cellHeight;

				this.cellsGroup.add(cell);
				this.cells[column][row] = cell;

				kind = kind == CellKind.White ? CellKind.Black : CellKind.White;
			}
			kind = kind == CellKind.White ? CellKind.Black : CellKind.White;
		}
	}

	initChips(kind) {
		let rows = 3;

		if (kind == TeamKind.White) {
			for (let row = 0; row < rows; row++) {
				for (let column = (row == 1 ? 0 : 1); column < this.columns; column += 2) {
					let chip = new Chip(this.game, kind, column, row);

					chip.x = column * this.cellWidth;
					chip.y = row * this.cellHeight;

					this.chipsGroup.add(chip);
					this.chips.push(chip);
				}
			}

		} else {
			for (let row = this.rows - rows; row < this.rows; row++) {
				for (let column = (row == this.rows - rows + 1 ? 1 : 0); column < this.columns; column += 2) {
					let chip = new Chip(this.game, kind, column, row);

					chip.x = column * this.cellWidth;
					chip.y = row * this.cellHeight;

					this.chipsGroup.add(chip);
					this.chips.push(chip);
				}
			}
		}
	}


	showStartTarget(column, row) {
		this.startTarget.x = column * this.cellWidth + this.cellWidth / 2;
		this.startTarget.y = row * this.cellHeight + this.cellHeight / 2;
		this.startTarget.show();
	}

	hideStartTarget() {
		this.startTarget.hide();
	}

	showEndTarget(column, row) {
		this.endTarget.x = column * this.cellWidth + this.cellWidth / 2;
		this.endTarget.y = row * this.cellHeight + this.cellHeight / 2;
		this.endTarget.show();
	}

	hideEndTarget() {
		this.endTarget.hide();
	}

	onMouseUp(pointer) {
		let position = pointer.position;

		let cellWidth = this.cellWidth;
		let cellHeight = this.cellHeight;

		let column = parseInt(position.x / cellWidth);
		let row = parseInt(position.y / cellHeight) - 1;

		let chip = this.getChipByCell(column, row);

		if (chip && chip != this.currChip) {
			if (chip.kind == this.currTeam) {
				this.addAction(new SelectAction(this.currTeam, column, row));
			}

		} else if (!chip && this.currChip) {
			if (this.currPaths.length > 0) {
				let dirX = this.currChip.column > column ? 1 : 0;
				let dirY = this.currChip.row > row ? this.currTeam == TeamKind.White ? 1 : 0 : this.currTeam == TeamKind.White ? 0 : 1;
				let index = 0;

				if (dirX == 0 && dirY == 0) {
					index = 0;

				} else if (dirX == 1 && dirY == 0) {
					index = 1

				} else if (dirX == 0 && dirY == 1) {
					index = 2

				} else if (dirX == 1 && dirY == 1) {
					index = 3;
				}

				let path = this.currPaths[index];
				if (path) {
					let cells = path.cells;

					if (_.findWhere(cells, {column: column, row: row})) {
						this.addAction(new MoveAction(this.currTeam, this.currChip.column, this.currChip.row, column, row));

					} else {
						this.unselectChip();
					}
				}
			}
		}
	}

	findPaths(fromColumn, fromRow, crowned) {
		let paths = [];

		let getNext = (fromColumn, fromRow, dir) => {
			let column = fromColumn;
			let row = fromRow;

			if (dir < 0) {
				column += 1;

			} else {
				column -= 1;
			}
			row += this.currTeam == TeamKind.Black ? -1 : 1;

			return {column: column, row: row};
		};
		let getPrev = (fromColumn, fromRow, dir) => {
			let column = fromColumn;
			let row = fromRow;

			if (dir < 0) {
				column += 1;

			} else {
				column -= 1;
			}
			row -= this.currTeam == TeamKind.Black ? -1 : 1;

			return {column: column, row: row};
		};

		let findPath = (fromColumn, fromRow, dir, crowned, nextFunc) => {
			let column = fromColumn;
			let row = fromRow;

			let path = {
				cells: [],
				chips: []
			};

			while (1) {
				let next = nextFunc(column, row, dir);

				column = next.column;
				row = next.row;

				let cell = this.getCell(column, row);
				let chip = this.getChipByCell(column, row);

				if (cell) {
					if (chip) {
						if (chip.kind != this.currTeam) {
							let nextPos = nextFunc(column, row, dir);
							let nextCell = this.getCell(nextPos.column, nextPos.row);
							let nextChip = this.getChipByCell(nextPos.column, nextPos.row);

							if (nextCell && !nextChip) {
								path.chips.push(chip);
								path.cells.push(cell, nextCell);

							} else if (nextCell && nextChip) {
								break;
							}

						} else {
							break;
						}

					} else {
						path.cells.push(cell);
					}

				} else {
					break;
				}

				if (!crowned) {
					break;
				}
			}

			return path.cells.length > 0 ? path : null;
		};

		let leftNext = findPath(fromColumn, fromRow, -1, crowned, getNext);
		let rightNext = findPath(fromColumn, fromRow, 1, crowned, getNext);

		if (leftNext) {
			paths[0] = leftNext;
		}
		if (rightNext) {
			paths[1] = rightNext;
		}

		if (crowned) {
			let leftPrev = findPath(fromColumn, fromRow, -1, crowned, getPrev);
			let rightPrev = findPath(fromColumn, fromRow, 1, crowned, getPrev);

			if (leftPrev) {
				paths[2] = leftPrev;
			}
			if (rightPrev) {
				paths[3] = rightPrev;
			}
		}

		return paths;
	}

	selectChip(column, row, onComplete) {
		this.unselectChip();
		this.showStartTarget(column, row);

		this.currChip = this.getChipByCell(column, row);
		if (onComplete) {
			onComplete();
		}

		this.currPaths = this.findPaths(column, row, this.currChip.crowned);

		for (let i = 0; i < this.currPaths.length; i++) {
			let path = this.currPaths[i];
			if (path) {
				let cells = path.cells;

				for (let j = 0; j < cells.length; j++) {
					cells[j].alpha = 0.9;
				}
			}
		}
	}

	moveChip(fromColumn, fromRow, toColumn, toRow, onComplete) {
		let dirX = fromColumn < toColumn ? 0 : 1;
		let dirY = fromRow > toRow ? this.currTeam == TeamKind.White ? 1 : 0 : this.currTeam == TeamKind.White ? 0 : 1;

		let index = 0;

		if (dirX == 0 && dirY == 0) {
			index = 0;

		} else if (dirX == 1 && dirY == 0) {
			index = 1

		} else if (dirX == 0 && dirY == 1) {
			index = 2

		} else if (dirX == 1 && dirY == 1) {
			index = 3;
		}

		let path = this.currPaths[index];
		let cells = path.cells;
		let chips = path.chips;

		if (cells.length > 0) {
			this.showEndTarget(toColumn, toRow);

			let tween = this.game.add.tween(this.currChip);
			let time = 100;

			for (let i = 0; i < cells.length; i++) {
				let cell = cells[i];

				tween.to({x: cell.x, y: cell.y}, time, Phaser.Easing.Linear.None);

				if (cell.row == toRow && cell.column == toColumn) {
					break;
				}
			}

			this.chips = _.difference(this.chips, chips);

			tween.onComplete.add(() => {
				for (let i = 0; i < chips.length; i++) {
					let chip = chips[i];

					chip.playFadeOff(() => {
						this.chipsGroup.remove(chip);
					});
				}

				if (this.currTeam == TeamKind.White) {
					this.scoreBlack += chips.length;

					if (toRow == this.rows - 1) {
						this.currChip.crowned = true;
					}

				} else {
					this.scoreWhite += chips.length;

					if (toRow == 0) {
						this.currChip.crowned = true;
					}
				}

				this.currChip.row = toRow;
				this.currChip.column = toColumn;

				this.unselectChip();

				if (onComplete) {
					onComplete();
				}
			});
			tween.start();

		} else {
			if (onComplete) {
				onComplete();
			}
		}
	}

	unselectChip() {
		if (this.currChip) {
			this.hideStartTarget();
			this.hideEndTarget();
			this.currChip = null;
		}

		for (let column = 0; column < this.columns; column++) {
			for (let row = 0; row < this.rows; row++) {
				this.cells[column][row].alpha = 1;
			}
		}
	}

	addAction(action) {
		this.actions.push(action);
		if (!this.doingAction) {
			this.nextAction();
		}
	}

	nextAction() {
		if (this.actions.length - 1 > this.actionIdx) {
			this.doingAction = true;

			let action = this.actions[++this.actionIdx];

			console.log(action.kind);

			if (action.kind == ActionKind.Select) {
				this.selectChip(action.column, action.row, () => {
					this.nextAction();
				});

			} else if (action.kind == ActionKind.Move) {
				this.moveChip(action.fromColumn, action.fromRow, action.toColumn, action.toRow, () => {
					this.swapTeam(() => {
						this.nextAction();
					});
				});
			}

		} else {
			this.doingAction = false;
		}
	}

	swapTeam(callback) {
		this.currTeam = this.currTeam == TeamKind.White ? TeamKind.Black : TeamKind.White;
		this.playAttentionChips(callback);
	}

	playAttentionChips(callback) {
		for (let i = 0; i < this.chips.length; i++) {
			let chip = this.chips[i];
			if (chip.kind == this.currTeam) {
				chip.alpha = 1;
				chip.playAttention(i * 30, callback);

			} else {
				chip.alpha = 0.6;
			}
		}
	}

	getCell(column, row) {
		if (column < this.cells.length && column > -1) {
			if (row < this.cells[column].length && row > -1) {
				return this.cells[column][row];
			}
		}
		return null;
	}

	getChipByCell(column, row) {
		return _.findWhere(this.chips, {column: column, row: row});
	}
}