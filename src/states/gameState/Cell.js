import CellKind from "./CellKind";

export default class Cell extends Phaser.Sprite {
	constructor(game, kind, column, row) {
		super(game, 0, 0, 'atlas_0');

		this.kind = kind;
		this.column = column;
		this.row = row;

		this.animations.add(CellKind.White, ['tile_01.png'], 1, true);
		this.animations.add(CellKind.Black, ['tile_02.png'], 1, true);
		this.animations.play(kind);
	}
}