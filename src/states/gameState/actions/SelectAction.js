import Action from "./Action";
import ActionKind from "./ActionKind";

export default class SelectAction extends Action {
	constructor(team, column, row) {
		super(team, ActionKind.Select);

		this.column = column;
		this.row = row;
	}
}
