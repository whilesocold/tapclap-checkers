import Action from "./Action";
import ActionKind from "./ActionKind";

export default class MoveAction extends Action {
	constructor(team, fromColumn, fromRow, toColumn, toRow) {
		super(team, ActionKind.Move);

		this.fromColumn = fromColumn;
		this.fromRow = fromRow;
		this.toColumn = toColumn;
		this.toRow = toRow;
	}
}
