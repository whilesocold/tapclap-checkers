export default class Action {
	constructor(team, kind) {
		this.team = team;
		this.kind = kind;
	}
}
