const ActionKind = {
	Select: 'select',
	Move: 'move'
};

export default ActionKind;