import GameState from 'states/GameState';

class Game extends Phaser.Game {
	constructor() {
		super(800, 900, Phaser.AUTO, 'content', null);

		this.state.add('GameState', GameState, true);
	}
}

window.game = new Game();
